'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var TodoSchema = new Schema({
  text: {
    type: String,
    required: true
  },
  done: {
    type: Boolean
  }
});


TodoSchema.statics.load = function(id, cb) {
  this.findOne({
    _id: id
  }).exec(cb);
};

mongoose.model('Todo', TodoSchema);
