'use strict';

var mongoose = require('mongoose'),
  Todo = mongoose.model('Todo'),
  _ = require('lodash');

exports.todo = function(req, res, next, id) {
  Todo.load(id, function(err, todo) {
    if (err) return next(err);
    if (!todo) return next(new Error('Failed to load todo ' + id));
    req.todo = todo;
    next();
  });
};

exports.create = function(req, res) {
  var todo = new Todo(req.body);

  todo.save(function(err) {
    if (err) {
      return res.status(500).json({
        error: 'Cannot save the todo'
      });
    }
    res.json(todo);

  });
};

exports.update = function(req, res) {
  var todo = req.todo;

  todo = _.extend(todo, req.body);

  todo.save(function(err) {
    if (err) {
      return res.status(500).json({
        error: 'Cannot update the todo'
      });
    }
    res.json(todo);

  });
};

exports.destroy = function(req, res) {
  var todo = req.todo;

  todo.remove(function(err) {
    if (err) {
      return res.status(500).json({
        error: 'Cannot delete the todo'
      });
    }
    res.json(todo);

  });
};

exports.show = function(req, res) {
  res.json(req.todo);
};


exports.all = function(req, res) {
  Todo.find().exec(function(err, todos) {
    if (err) {
      return res.status(500).json({
        error: 'Cannot list the todos'
      });
    }
    res.json(todos);

  });
};
