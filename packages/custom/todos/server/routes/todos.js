'use strict';


var todos = require('../controllers/todos');


module.exports = function(Todos, app, auth, database) {

  app.route('/todos')
    .get(todos.all)
    .post(auth.requiresLogin, todos.create);
  app.route('/todos/:todoId')
    .get(auth.isMongoId, todos.show)
    .put(auth.isMongoId, auth.requiresLogin, todos.update)
    .delete(auth.isMongoId, auth.requiresLogin, todos.destroy);

  app.param('todoId', todos.todo);



  app.get('/todos/example/anyone', function(req, res, next) {
    res.send('Anyone can access this');
  });

  app.get('/todos/example/auth', auth.requiresLogin, function(req, res, next) {
    res.send('Only authenticated users can access this');
  });

  app.get('/todos/example/admin', auth.requiresAdmin, function(req, res, next) {
    res.send('Only users with Admin role can access this');
  });

  app.get('/todos/example/render', function(req, res, next) {
    Todos.render('index', {
      package: 'todos'
    }, function(err, html) {
      //Rendering a view from the Package server/views
      res.send(html);
    });
  });
};
