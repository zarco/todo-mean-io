'use strict';

angular.module('mean.todos').controller('TodosController', ['$scope', '$stateParams', '$location', 'Global', 'Todos',
  function($scope, $stateParams, $location, Global, Todos) {
    $scope.global = Global;

    $scope.create = function(isValid) {
      if (isValid) {
        var todo = new Todos({
          text: this.text,
          done: false
        });
        todo.$save(function(response) {
          $scope.todos.push(response);
        });

        this.text = '';
      } else {
        $scope.submitted = true;
      }
    };

    $scope.remove = function(todo) {
      console.log('Removing todo...');
      if (todo) {
        todo.$remove(function(response) {
          for (var i in $scope.todos) {
            if ($scope.todos[i] === todo) {
	             $scope.todos.splice(i,1);
            }
          }
        });
      } else {
        $scope.todo.$remove(function(response) {
        });
      }
    };


    $scope.find = function() {
      Todos.query(function(todos) {
        $scope.todos = todos;
      });
    };

  }
]);
